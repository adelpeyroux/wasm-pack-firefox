Based on: https://github.com/rnsloan/dockerfiles

# Image with much of the tooling required to use wasm-pack:

Built from image rust:lastest

- built from image [rust:latest](https://hub.docker.com/_/rust/)
- [wasm-pack 0.10.1](https://rustwasm.github.io/wasm-pack/)
- [cargo-generate 0.10.3](https://crates.io/crates/cargo-generate)
- [nodejs v10.13.0 LTS](https://nodejs.org/en/)
- [yarn 3.1.0](https://yarnpkg.com/en/)
- [Firefox-esr 78](https://www.mozilla.org/en-US/firefox/new/)

Example:

```
docker run -it --name wasm -v ${PWD}/:/usr/src/ -w usr/src/ -p 8080:8080 rnsloan/wasm-pack:1.0.0 /bin/bash
```

## webpack-dev-server

When running webpack-dev-server from inside a docker container explicitly set the host to `0.0.0.0`: `webpack-dev-server --host 0.0.0.0 --port 8080`
